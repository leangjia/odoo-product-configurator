# -*- coding: utf-8 -*-
{
    'name': 'Product Configurator Sale',
    'version': '9.0.1.0.0',
    'category': 'Generic Modules/Sale',
    'summary': 'Product configuration interface modules for Sale',
    'author': 'Pledra',
    'license': 'AGPL-3',
    'website': 'http://www.pledra.com/',
    'depends': ['sale', 'product_configurator'],
    "data": [
        'data/menu_product.xml',
        'views/sale_view.xml',
    ],
    'demo': [],
    'images': [],
    'test': [],
    'installable': True,
    'auto_install': False,
}
