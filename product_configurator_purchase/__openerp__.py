# -*- coding: utf-8 -*-
{
    'name': 'Product Configurator Purchase',
    'version': '9.0.1.0.0',
    'category': 'Generic Modules/Purchase',
    'summary': 'Product configuration interface for Purchase',
    'author': 'Pledra',
    'license': 'AGPL-3',
    'website': 'http://www.pledra.com/',
    'depends': ['purchase', 'product_configurator'],
    "data": [
        'data/menu_product.xml',
        'views/purchase_view.xml',
    ],
    'demo': [],
    'images': [],
    'test': [],
    'installable': True,
    'auto_install': False,
}
